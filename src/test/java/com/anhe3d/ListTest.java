package com.anhe3d;

import org.junit.gen5.api.Test;
import org.junit.gen5.junit4.runner.JUnit5;
import org.junit.runner.RunWith;

import java.util.*;

/**
 * Created by kevinhung on 5/17/16.
 */
@RunWith(JUnit5.class)
public class ListTest {

    @Test
    public void testListAdding () {
        List<LinkedList<Integer>> sliceArrays = new ArrayList<>(10);
        int counter = 0;
        while (counter < 10) {
            LinkedList<Integer> sliceArray = new LinkedList<>();
            sliceArray.add(counter*10);
            sliceArray.add(counter*10+1);
            sliceArrays.add(sliceArray);
            counter++;
        }
        Map<String, LinkedList<Integer>> map = new HashMap<>();
        map.put("111", sliceArrays.get(1));
        map.put("222", sliceArrays.get(2));

        sliceArrays.get(0).addAll(sliceArrays.get(1));
//        sliceArrays.remove(map.get("111"));
        System.out.println(sliceArrays);




    }
}
