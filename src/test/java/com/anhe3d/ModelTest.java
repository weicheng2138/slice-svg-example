package com.anhe3d;

import com.anhe3d.domain.Facet;
import com.anhe3d.util.SliceAlgorithm;
import com.anhe3d.util.StlLoader;
import com.anhe3d.util.SvgImageGenerator;
import org.apache.batik.apps.rasterizer.SVGConverterException;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.commons.io.FileUtils;
import org.junit.gen5.api.Test;
import org.junit.gen5.junit4.runner.JUnit5;
import org.junit.runner.RunWith;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by kevinhung on 5/23/16.
 */
@RunWith(JUnit5.class)
public class ModelTest {

    Path stlFilePath;
    List<Facet> facets;

    @Test
    public void testSTL() throws IOException, SAXException, SVGConverterException, TranscoderException {
        stlFilePath = Paths.get("src/main/resources/3D-models/fillenium_malcon.stl");
        FileInputStream file = FileUtils.openInputStream(stlFilePath.toFile());
        String header = StlLoader.loadBinaryHeader(file);
        long facetCount = StlLoader.loadBinaryFacetCount(file);
        facets = StlLoader.loadBinaryContent(file, facetCount);





        FileUtils.deleteDirectory(Paths.get("svg-output-files/svg").toFile());
        FileUtils.deleteDirectory(Paths.get("svg-output-files/pdf").toFile());

        SliceAlgorithm.processAlgorithm(0.1, facets);


        SliceAlgorithm.generateSvgContour("svg-output-files/svg");
//        SliceAlgorithm.generateSvgColoredContour("svg-output-files/svg");


        SvgImageGenerator svgImageGenerator = new SvgImageGenerator();
        svgImageGenerator.exportPDF("/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/svg",
                "/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/pdf");

        svgImageGenerator.mergePDF("/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/pdf", "svg-output-files/mergedPDF.pdf");


    }

}
