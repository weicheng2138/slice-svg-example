package com.anhe3d;

import com.anhe3d.domain.Facet;
import com.anhe3d.util.StlLoader;
import org.apache.commons.io.FileUtils;
import org.junit.gen5.api.Test;
import org.junit.gen5.junit4.runner.JUnit5;
import org.junit.runner.RunWith;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by kevinhung on 5/13/16.
 */
@RunWith(JUnit5.class)
public class LoaderTest {

    @Test
    public void testInit () throws IOException {
        List<Facet> facets;

        //Loader stl into facets
        Path stlFilePath;
        if (false) { // ascii stl
            stlFilePath = Paths.get("src/main/resources/3D-models/tetrahedron.stl");
//            facets = StlLoader.parseAscii(StlLoader.parseAsciiWithoutHeader(stlFilePath));//.get("tetrahedron");
//            stlFilePath = Paths.get("src/main/resources/3D-models/CylinderHead-ascii.stl");
            facets = StlLoader.parseAsciiWithoutHeader(StlLoader.loadAscii(stlFilePath));//.get("CylinderHead-ascii-OpenCASCADE-InteractiveMesh");
        } else { // binary stl
//            stlFilePath = Paths.get("src/main/resources/3D-models/3DBenchy.stl");
//            stlFilePath = Paths.get("src/main/resources/3D-models/Calibration Pyramid.stl");
//            stlFilePath = Paths.get("src/main/resources/3D-models/Rubbish_Bin_By_Berken.stl");
            stlFilePath = Paths.get("src/main/resources/3D-models/colored_Binary.stl");
            FileInputStream file = FileUtils.openInputStream(stlFilePath.toFile());
            String header = StlLoader.loadBinaryHeader(file);
            System.out.println("loading start");

            long facetCount = StlLoader.loadBinaryFacetCount(file);
            facets = StlLoader.loadBinaryContent(file, facetCount);
            System.out.println(facetCount);

        }

        //Present the file you load
//        facets.forEach( e -> {
//            System.out.println("(" + e.getVertexes()[0].getCoord().getX()+", "+e.getVertexes()[0].getCoord().getY()+", "+e.getVertexes()[0].getCoord().getZ()+")");
//            System.out.println("(" + e.getVertexes()[1].getCoord().getX()+", "+e.getVertexes()[1].getCoord().getY()+", "+e.getVertexes()[1].getCoord().getZ()+")");
//            System.out.println("(" + e.getVertexes()[2].getCoord().getX()+", "+e.getVertexes()[2].getCoord().getY()+", "+e.getVertexes()[2].getCoord().getZ()+")\n");
//
//        });
//        System.out.println("*************************");

        facets.forEach( e -> {
            System.out.println("(" + e.getVertexes()[0].getColor()+", "+e.getVertexes()[1].getColor()+", "+e.getVertexes()[2].getColor()+")");
        });


        System.out.println("*************************");


//        facets.forEach( facet -> {
//            System.out.println(facet.getEdges()[0].getEdgeType() +" " + facet.getEdges()[1].getEdgeType()+ " " +facet.getEdges()[2].getEdgeType());
//            System.out.println(facet.getEdges()[0].toString());
//        });


    }


}
