package com.anhe3d;

import com.anhe3d.util.PillShapeGenerator;
import org.apache.batik.apps.rasterizer.SVGConverterException;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.commons.io.FileUtils;
import org.junit.gen5.api.Test;
import org.junit.gen5.junit4.runner.JUnit5;
import org.junit.runner.RunWith;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 * Created by kevinhung on 9/12/16.
 */
@RunWith(JUnit5.class)
public class PillTest {

    @Test
    public void testPillProducing() throws IOException, SAXException, SVGConverterException, TranscoderException {
        FileUtils.deleteDirectory(Paths.get("svg-output-files/svg").toFile());
        FileUtils.deleteDirectory(Paths.get("svg-output-files/pdf").toFile());
        PillShapeGenerator pillShapeGenerator = new PillShapeGenerator();

        double diameter = 50;
        double thickness = 4;

        ArrayList<Integer> specResult = pillShapeGenerator.inputPillSpec(diameter, thickness, 2, 1);
        System.out.println(specResult);

        pillShapeGenerator.generatePills("svg-output-files/svg", 1, 1, 1, 1, specResult);
        pillShapeGenerator.exportPDF("/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/svg",
                "/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/pdf");

    }



}