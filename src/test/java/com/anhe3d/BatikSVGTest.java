package com.anhe3d;

import com.anhe3d.util.SvgImageGenerator;
import org.apache.batik.apps.rasterizer.SVGConverterException;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.commons.io.FileUtils;
import org.junit.gen5.api.Test;
import org.junit.gen5.junit4.runner.JUnit5;
import org.junit.runner.RunWith;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.nio.file.Paths;


/**
 * Created by kevinhung on 5/22/16.
 */
@RunWith(JUnit5.class)
public class BatikSVGTest {

    @Test
    public void createPDF() throws IOException, SAXException, SVGConverterException, TranscoderException {
        FileUtils.deleteDirectory(Paths.get("svg-output-files/svg").toFile());
        FileUtils.deleteDirectory(Paths.get("svg-output-files/pdf").toFile());
        FileUtils.deleteDirectory(Paths.get("svg-output-files/png").toFile());
        SvgImageGenerator svgImageGenerator = new SvgImageGenerator();
        svgImageGenerator.generateCMYK("svg-output-files/svg", 100);
//        svgImageGenerator.exportPNG("/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/svg",
//                "/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/png");
        svgImageGenerator.exportPDF("/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/svg",
                "/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/pdf");


    }



}
