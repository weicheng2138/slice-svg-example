package com.anhe3d.util;

import com.anhe3d.domain.Edge;
import com.anhe3d.domain.IntersectionStructure;
import com.anhe3d.domain.Vertex;
import com.anhe3d.domain.javax.vecmath.Point3d;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by kevinhung on 5/16/16.
 */
public class SliceUtil {

    public static Vertex computeIntersectionVertex(double facetZMed, Edge interEdge, int sCounter, double sLower, double sUpper, double sMedium) {
        double sRange = sUpper - sLower;
        Vertex interVertex = new Vertex();

        //TODO modify to be more efficient
//        System.out.println(interEdge.getVertexes()[0].getCoord().z > interEdge.getVertexes()[1].getCoord().z
//                ? interEdge.getVertexes()[0].getCoord() : interEdge.getVertexes()[1].getCoord());
        Point3d point1 = interEdge.getVertexes()[0].getCoord().z > interEdge.getVertexes()[1].getCoord().z
                ? interEdge.getVertexes()[0].getCoord() : interEdge.getVertexes()[1].getCoord();
        Point3d point2 = interEdge.getVertexes()[0].getCoord().z < interEdge.getVertexes()[1].getCoord().z
                ? interEdge.getVertexes()[0].getCoord() : interEdge.getVertexes()[1].getCoord();

//        System.out.println("point1: " + point1);
//        System.out.println("point2: " + point2);


//        System.out.println("***" + interEdge.toOnlyVertexes());
        if (point1.z != facetZMed && point2.z != facetZMed && sCounter < (int) sMedium + 1) {
            Point3d pointMed = interEdge.getPoint3dByHeight(facetZMed);
//            System.out.println("pointMed" + pointMed);
            interVertex.setCoord(getIntersectionPoint(point2, pointMed, sCounter, sLower, sRange));
        } else if (point1.z != facetZMed && point2.z != facetZMed && sCounter >= (int) sMedium + 1) {
            Point3d pointMed = interEdge.getPoint3dByHeight(facetZMed);
//            System.out.println("pointMed" + pointMed);
            interVertex.setCoord(getIntersectionPoint(pointMed, point1, sCounter, sLower, sRange));
        }
        else {
            interVertex.setCoord(getIntersectionPoint(point2, point1, sCounter, sLower, sRange));
        }

//        System.out.println(point3d.toString());
        return interVertex;
    }

    private static Point3d getIntersectionPoint(Point3d pointLow, Point3d pointHigh, int sCounter, double sLower, double sRange) {
        double x = pointLow.x + ((sCounter-sLower) * (pointHigh.x - pointLow.x) / sRange);
        double y = pointLow.y + ((sCounter-sLower) * (pointHigh.y - pointLow.y) / sRange);
        double z = pointLow.z + ((sCounter-sLower) * (pointHigh.z - pointLow.z) / sRange);
        return new Point3d(x, y, z);
    }

    public static void constructContourList(IntersectionStructure IS, List<LinkedList> sliceArrays, int k) {
        List<LinkedList<IntersectionStructure>> sliceArray = sliceArrays.get(k);
        if (sliceArray.size() == 0) {
            LinkedList<IntersectionStructure> newContourList = new LinkedList<>();
            newContourList.add(IS);
            sliceArray.add(newContourList);
        } else {

            Map<String, LinkedList<IntersectionStructure>> attachedContourList = new HashMap<>();
            for (LinkedList<IntersectionStructure> contourList: sliceArray) {

                IntersectionStructure headIS = contourList.getFirst();
                IntersectionStructure tailIS = contourList.getLast();


//                System.out.println(IS.getEdgePair().getForwardEdge().toOnlyVertexes() + "\n" +
//                        headIS.getEdgePair().getBackwardEdge().toOnlyVertexes() + "\n");

                //Check whether it should be the current contourList's head or tail
                //Has already eliminated the one contourList with head and back situation
                //
                if (IS.getEdgePair().getBackwardEdge().isEqual(headIS.getEdgePair().getForwardEdge())) {
                    attachedContourList.put("headOfContour", contourList);
                } else if (IS.getEdgePair().getForwardEdge().isEqual(tailIS.getEdgePair().getBackwardEdge())) {
                    attachedContourList.put("backOfContour", contourList);
                }

            }
//            System.out.println(attachedContourList.size());

            //According to the size of attachedContourList
            if (attachedContourList.size() == 2) {
//                System.out.println("#attachedContourList in size of 2");
//                if (!attachedContourList.get("headOfContour").getFirst().getInterVertex().isEqualZ(IS.getInterVertex())) {
//                    System.out.println("counter: "+ k);
//                    System.out.println("@@@2IS ---> " + IS.getInterVertex());
//                    System.out.println("@@@2Contour head ---> " + attachedContourList.get("headOfContour").getFirst().getInterVertex());
//                }
                attachedContourList.get("headOfContour").addFirst(IS);
//
                attachedContourList.get("backOfContour").addAll(attachedContourList.get("headOfContour"));
                sliceArray.remove(attachedContourList.get("headOfContour"));

            } else if (attachedContourList.size() == 1) {
//                System.out.println("#attachedContourList in size of 1");
//                System.out.println("sliceArray size: " + sliceArray.size());
//                System.out.println(attachedContourList.get("headOfContour"));
//                System.out.println(attachedContourList.get("backOfContour"));


                if (attachedContourList.get("headOfContour") != null) {
//                    System.out.println("HEAD");
                    if (!attachedContourList.get("headOfContour").getFirst().getInterVertex().isEqualZ(IS.getInterVertex())) {
//                        System.out.println("counter: "+ k);
//                        System.out.println("@@@1IS ---> " + IS.getInterVertex());
//                        System.out.println("@@@1Contour head ---> " + attachedContourList.get("headOfContour").getFirst().getInterVertex());
                    }
                    attachedContourList.get("headOfContour").addFirst(IS);
                } else if (attachedContourList.get("backOfContour") != null) {
//                    System.out.println("BACK");
                    if (!attachedContourList.get("backOfContour").getFirst().getInterVertex().isEqualZ(IS.getInterVertex())) {
//                        System.out.println("counter: "+ k);
//                        System.out.println("@@@1IS ---> " + IS.getInterVertex());
//                        System.out.println("@@@1Contour back ---> " + attachedContourList.get("backOfContour").getFirst().getInterVertex());
                    }
                    attachedContourList.get("backOfContour").addLast(IS);
                }

            } else if (attachedContourList.size() == 0) {
//                System.out.println("#attachedContourList in size of 0");
                LinkedList<IntersectionStructure> newContourList = new LinkedList<>();
                newContourList.add(IS);
                sliceArray.add(newContourList);
            } else {
                System.out.println("Something Wrong with you attachedContourList");
            }

//            System.out.println("sliceArray size: " + sliceArray.size());

        }
    }
}
