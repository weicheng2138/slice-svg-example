package com.anhe3d;

import com.anhe3d.domain.Edge;
import com.anhe3d.domain.EdgePair;
import com.anhe3d.domain.Facet;
import com.anhe3d.domain.IntersectionStructure;
import com.anhe3d.util.SliceUtil;
import com.anhe3d.util.StlLoader;
import com.anhe3d.util.SvgImageGenerator;
import org.apache.batik.apps.rasterizer.SVGConverterException;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by kevinhung on 5/13/16.
 */
public class Main {

//    public static final Double Z_INCREMENT = 1E-3;
//    public static final Double Z_INCREMENT = 0.1;
    public static final Integer Z_INCREMENT = 1;

    public static void main(String[] args) throws IOException, SAXException, SVGConverterException, TranscoderException {
        final Logger logger = LoggerFactory.getLogger(Main.class);

        //Load stl file to facets
        List<Facet> facets;
//        Path stlFilePath = Paths.get("src/main/resources/3D-models/tetrahedron-test.stl");
//        Path stlFilePath = Paths.get("src/main/resources/3D-models/tetrahedron.stl");
//        Path stlFilePath = Paths.get("src/main/resources/3D-models/slottedDisk_Ascii.stl");
//        Path stlFilePath = Paths.get("src/main/resources/3D-models/CylinderHead-ascii.stl");
//        Path stlFilePath = Paths.get("src/main/resources/3D-models/Egg_Holder_Stand_Ascii.stl");
//        facets = StlLoader.parseAsciiWithoutHeader(StlLoader.loadAscii(stlFilePath));


//        Path stlFilePath = Paths.get("src/main/resources/3D-models/CylinderHead-binary.stl");
//        Path stlFilePath = Paths.get("src/main/resources/3D-models/CylinderHead-binary_fixed.stl");
//        Path stlFilePath = Paths.get("src/main/resources/3D-models/3DBenchy_Binary.stl");
//        Path stlFilePath = Paths.get("src/main/resources/3D-models/cylindercurve_Binary.stl");
        Path stlFilePath = Paths.get("src/main/resources/3D-models/Pyrimid with Shell-b.STL");
//        Path stlFilePath = Paths.get("src/main/resources/3D-models/Fantasy_Castle.stl");
//        Path stlFilePath = Paths.get("src/main/resources/3D-models/fillenium_malcon.stl");
//        Path stlFilePath = Paths.get("src/main/resources/3D-models/colored_Binary.stl");

        logger.info("Start Loading...");
        FileInputStream file = FileUtils.openInputStream(stlFilePath.toFile());
        String header = StlLoader.loadBinaryHeader(file);


        long startTime = System.currentTimeMillis();
        long facetCount = StlLoader.loadBinaryFacetCount(file);
        facets = StlLoader.loadBinaryContent(file, facetCount);

        long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
        logger.info("Time for loading model: " + totalTime);







        //Model analyze for space spec before the algorithm
        //TODO Preprocess
        double modelZMax = 0;
        double modelZMin = Double.MAX_VALUE;
        for(Facet facet: facets) {
            double facetZMin = facet.getEdges()[0].getZMin();
            double facetZMax = facet.getEdges()[0].getZMax();
//            System.out.println(facetZMin + " facet " + facetZMax);

            modelZMax = facetZMax > modelZMax ? facetZMax : modelZMax;
            modelZMin = facetZMin < modelZMin ? facetZMin : modelZMin;
        }
        System.out.println("********************************************************");
        System.out.println("Model Minimum Height: " + modelZMin + "; Model Maximum Height: " + modelZMax);
        System.out.println("Facets Count: " + facets.size());

        //Compute total Slice quantity
        double sTotal = (modelZMax - modelZMin) / Z_INCREMENT;
        System.out.println("sTotal: " + sTotal);
        System.out.println("********************************************************");





        //Zhang's Algorithm start from here
        //Initialize edgePair Storage and Slice Array
        startTime = System.currentTimeMillis();
        System.out.println("Zhang's Algorithm start...\n");
        List<LinkedList> sliceArrays = new ArrayList<>((int)sTotal + 1);
        int counter = 0;
        while (counter++ < (int)sTotal + 1) {
            sliceArrays.add(new LinkedList<>());
        }

        for(Facet facet: facets) {
            double facetZMin = facet.getEdges()[0].getZMin();
            double facetZMax = facet.getEdges()[0].getZMax();
            double facetZMed = facet.getEdges()[1].getZMax();
//            System.out.println(facetZMin + " " + facetZMed + " " + facetZMax);

            if (facetZMin != facetZMax) {
//                System.out.println("\n(" + facet.getVertexes()[0].getCoord().getX()+", "+facet.getVertexes()[0].getCoord().getY()+", "+facet.getVertexes()[0].getCoord().getZ()+")");
//                System.out.println("(" + facet.getVertexes()[1].getCoord().getX()+", "+facet.getVertexes()[1].getCoord().getY()+", "+facet.getVertexes()[1].getCoord().getZ()+")");
//                System.out.println("(" + facet.getVertexes()[2].getCoord().getX()+", "+facet.getVertexes()[2].getCoord().getY()+", "+facet.getVertexes()[2].getCoord().getZ()+")");

                //Section 4.2
                //Calculate the unit height we are going to process
                double sMin = (facetZMin - modelZMin) / Z_INCREMENT;
                double sMed = (facetZMed - modelZMin) / Z_INCREMENT;
                double sMax = (facetZMax - modelZMin) / Z_INCREMENT;
//                System.out.println("sMin: "+sMin + " sMed: "+sMed +" sMax: "+sMax);

                //Section 4.3
                //Determine S1 and S2 for judging forward or backward edge
                EdgePair edgePair;
                Edge s1Edge = facet.getEdges()[0];
                Edge s2Edge = facet.getEdges()[1];
                Edge s3Edge = facet.getEdges()[2];
//                System.out.println(s1Edge.toOnlyVertexes()+"\n"
//                        + s2Edge.toOnlyVertexes()+"\n"
//                        + s3Edge.toOnlyVertexes());
                if (s1Edge.getVertexes()[0].getCoord().getZ() > s1Edge.getVertexes()[1].getCoord().getZ()) {
                    edgePair = new EdgePair(s1Edge, s2Edge);
                } else {
                    edgePair = new EdgePair(s2Edge, s1Edge);
                }
//                System.out.println("forward: " + edgePair.getForwardEdge().toOnlyVertexes());
//                System.out.println("backward: " + edgePair.getBackwardEdge().toOnlyVertexes());

                //Section 4.4
                //Solve interVertex and slicing plane and get IntersectionStructure
                int j = sMin > 0 ? (int)sMin + 1 : 0;
                int temp = (sMed > 0) ? (int)sMed + 1: 0;
                while (j < temp) {

                    //Construct IS
                    Edge interEdge = edgePair.getForwardEdge();
                    IntersectionStructure IS = new IntersectionStructure(
                            SliceUtil.computeIntersectionVertex(facetZMed, interEdge, j, sMin, sMed, sMed),
                            edgePair
                    );

                    //Construct contour list
//                    System.out.println("current j = " + j);
//                    System.out.println(IS);
                    SliceUtil.constructContourList(IS, sliceArrays, j);
                    j++;
                }









                //Section 4.3
                //Determine S1 and S3 for judging forward or backward edge
                if (s1Edge.getVertexes()[0].getCoord().getZ() > s1Edge.getVertexes()[1].getCoord().getZ()) {
                    edgePair = new EdgePair(s1Edge, s3Edge);
                } else {
                    edgePair = new EdgePair(s3Edge, s1Edge);
                }
//                System.out.println("forward: " + edgePair.getForwardEdge().toOnlyVertexes());
//                System.out.println("backward: " + edgePair.getBackwardEdge().toOnlyVertexes());

                //Section 4.4
                //Solve interVertex and slicing plane and get IntersectionStructure
                int k = sMed > 0 ? (int) sMed + 1 : 0;
                //Testing j-k should be 1 or 0, 1 is normal condition and 0 is with condition of sMin = SMed
//                if ((j-k) == 1) { System.out.println("@@ "+ (j-k)); }
                while (k < (int) sMax + 1) {
                    Edge interEdge = edgePair.getForwardEdge();
                    IntersectionStructure IS = new IntersectionStructure(
                            SliceUtil.computeIntersectionVertex(facetZMed, interEdge, k, sMed, sMax, sMed),
                            edgePair
                    );

                    //Construct contour list
//                    System.out.println("current k = " + k);
                    SliceUtil.constructContourList(IS, sliceArrays, k);
                    k++;
                }
//                System.out.println();
            }



        }

        endTime   = System.currentTimeMillis();
        totalTime = endTime - startTime;
        logger.info("Time for  Algorithm: " + totalTime);



        //Result presentation
//        int indexLayer = 10;
//        List<LinkedList<IntersectionStructure>> resultIndexArray = sliceArrays.get(indexLayer);
//        System.out.println("\nSlice height for a specific CCL: ");
//        resultIndexArray.get(0).forEach(is -> {
//            System.out.println(is.getInterVertex().getCoord().z);
//        });
//        System.out.println("count of contour: " + sliceArrays.get(indexLayer).size());


        startTime = System.currentTimeMillis();

        int indexLayer = 0;
        FileUtils.deleteDirectory(Paths.get("svg-output-files/svg").toFile());
        FileUtils.deleteDirectory(Paths.get("svg-output-files/pdf").toFile());
        SvgImageGenerator svgImageGenerator = new SvgImageGenerator();
        while (indexLayer++ < sliceArrays.size()-1) {
//            System.out.println(indexLayer);
            List<LinkedList<IntersectionStructure>> resultIndexArray = sliceArrays.get(indexLayer);
            svgImageGenerator.generateContour("svg-output-files/svg", indexLayer, resultIndexArray);
//            svgImageGenerator.generateColoredContour("svg-output-files/svg", indexLayer, resultIndexArray);
//            System.out.println("Number of contour of each layer: " + resultIndexArray.size());
        }
        System.out.println("Contour output complete!!!");
        endTime   = System.currentTimeMillis();
        totalTime = endTime - startTime;
        logger.info("Time for generate svg contour: " + totalTime);


//        startTime = System.currentTimeMillis();
//        svgImageGenerator.exportPDF("/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/svg", "/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/pdf");
//        svgImageGenerator.exportPNG("/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/svg/layer-001.svg", "/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/png/layer-test");

//        svgImageGenerator.mergePDF("/Users/kevinhung/IdeaProjects/slice-svg-example/svg-output-files/pdf", "svg-output-files/mergedPDF.pdf");

//        endTime   = System.currentTimeMillis();
//        totalTime = endTime - startTime;
//        logger.info("Time for generating pdf: " + totalTime);



    }
}
